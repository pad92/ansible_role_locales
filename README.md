# locales

Build locales and set defaults

## Requirements

none

## Role Variables

```
cts_role_locales:
  default: en_US.UTF-8
  generate:
    - en_US
    - en_US.ISO-8859-15
    - en_US.UTF-8
    - fr_FR@euro
    - fr_FR
    - fr_FR.UTF-8
```

## Dependencies

none

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - locales
```

